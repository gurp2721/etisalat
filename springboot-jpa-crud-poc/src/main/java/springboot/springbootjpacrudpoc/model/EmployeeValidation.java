package springboot.springbootjpacrudpoc.model;
/**
 * @author Gurpreet
 *
 */
public class EmployeeValidation {

	
	/**
	 * @param employee
	 * @return
	 */
	public static boolean employeeValidation(Employee employee) {
		return employee.getFirstName() != null && employee.getFirstName().length() > 2 
				&& employee.getLastName() != null && employee.getLastName().length() > 2
				&& employee.getSalary() != 0 && employee.getPhoneNumber() != 0 ;
	}
}
