package springboot.springbootjpacrudpoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import springboot.springbootjpacrudpoc.model.Employee;
/**
 * @author Gurpreet
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
